import Search from '@mui/icons-material/Search';
import School from '@mui/icons-material/School';
import Computer from '@mui/icons-material/Computer';
import Person from '@mui/icons-material/Person';
import DeviceHub from '@mui/icons-material/DeviceHub';
import Code from '@mui/icons-material/Code';
import Assignment from '@mui/icons-material/Assignment';
import * as React from 'react'; 
import Navbar from '../components/Navbar';
import styles from '../css/ProffesorMyStudents.module.css';



export default function ProffesorMyStudents(){
    let listOfPapers=[
        {
            name:"Facial detection",
            professor:"Liviu Popescu",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:"Euristic algorithms",
            professor:"Mihai Bogdan",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:" Image classification",
            professor:"Ana Ionescu",
            facultate:"Mathematics and CS",
            specializare:"Mathematics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:"Object detection",
            professor:"Maria Vlad",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:"Complex algorithms",
            professor:"Sergiu Dan",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        }
    ]

    let listOfPapers2=[
        {
            name:"Skin Disease",
            professor:"Victor Pop",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:"Eye Detection",
            professor:"Ada Damian",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:"Neural network",
            professor:"Teodora Maria",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:"Object detection",
            professor:"Denisa Bulgur",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:"Complex algorithms",
            professor:"Cristina Ionescu",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        }
    ]

    let listOfPapers3=[
        {
            name:"Special networking",
            professor:"Bogdan Cuc",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:"Euristic algorithms",
            professor:"Bob Vasile",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:" Image classification",
            professor:"Mihai Popescu",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:"Object detection",
            professor:"Roxana Creanga",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        },
        {
            name:"Complex algorithms",
            professor:"Scai Ion",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS",
            codStatus: "60%",
            docStatus: "40%"
        }
    ]

    let [search, setSearch] = React.useState();


    const Card= ({title, teacher, faculty, specialization, technologies, codStatus, docStatus}) => {
        return(
            <div className={styles.card}>
            <div className={styles.title}>
                <p>{title}</p>
            </div>
            <div className={styles.divider}></div>
            <div className={styles.checked}>
                <Person style={{color:'#4d79ff'}}/>
                <p>Student: {teacher}</p>
            </div>
            <div className={styles.smallDivider}></div>
            <div className={styles.checked}>
                <School style={{color:'red'}}/>
                <p>Faculty: {faculty}</p>
            </div>
            <div className={styles.smallDivider}></div>
            <div className={styles.checked}>
                <Computer style={{color:'greenyellow'}}/>
                <p>Specialization: {specialization}</p>
            </div>
            <div className={styles.smallDivider}></div>
            <div className={styles.checked}>
                <DeviceHub style={{color:'green'}}/>
                <p>Technologies: {technologies}</p>
            </div>
            <div className={styles.smallDivider}></div>
            <div className={styles.checked}>
                <Code style={{color:'blue'}}/>
                <p>Code status: {codStatus} done</p>
            </div>
            <div className={styles.smallDivider}></div>
            <div className={styles.checked}>
                <Assignment style={{color:'brown'}}/>
                <p>Documentation status: {docStatus} done</p>
            </div>
        </div>
        )  
    };
    
    return(
        <div className={styles.page}>
            <div className={styles.importedNavbar}>
                <Navbar page="mystudents" type="proffesor"/>
            </div>

            <div className={styles.StudentSearchBody}>
                <div className={styles.searchBar}>
                    <Search className={styles.searchImage}/>
                    <input placeholder='Look for the  name of a paper' className={styles.searchBox} onChange={(e)=>{setSearch(e.target.value)}}/>
                </div>

                <div className={styles.listOfCards}>
                    {
                        search ? 
                        (listOfPapers.filter((element)=>element.name.toLocaleLowerCase().includes(search.toLocaleLowerCase()))
                        .map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare} 
                            technologies={element.tehnologii}
                            codStatus={element.codStatus}
                            docStatus={element.docStatus}
                        /> )) 
                        : 
                        (listOfPapers.map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare}
                            technologies={element.tehnologii}
                            codStatus={element.codStatus}
                            docStatus={element.docStatus}
                        />))
                    }
                </div>
                <div className={styles.listOfCards}>
                    {
                        search ? 
                        (listOfPapers2.filter((element)=>element.name.toLocaleLowerCase().includes(search.toLocaleLowerCase()))
                        .map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare} 
                            technologies={element.tehnologii}
                            codStatus={element.codStatus}
                            docStatus={element.docStatus}
                        /> )) 
                        : 
                        (listOfPapers2.map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare}
                            technologies={element.tehnologii}
                            codStatus={element.codStatus}
                            docStatus={element.docStatus}
                        />))
                    }
                </div>
                <div className={styles.listOfCards}>
                    {
                        search ? 
                        (listOfPapers3.filter((element)=>element.name.toLocaleLowerCase().includes(search.toLocaleLowerCase()))
                        .map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare} 
                            technologies={element.tehnologii}
                            codStatus={element.codStatus}
                            docStatus={element.docStatus}
                        /> )) 
                        : 
                        (listOfPapers3.map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare}
                            technologies={element.tehnologii}
                            codStatus={element.codStatus}
                            docStatus={element.docStatus}
                        />))
                    }
                </div>
            </div>
        </div>
    )

}