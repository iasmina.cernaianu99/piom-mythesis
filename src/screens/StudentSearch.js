import Search from '@mui/icons-material/Search';
import School from '@mui/icons-material/School';
import Computer from '@mui/icons-material/Computer';
import Person from '@mui/icons-material/Person';
import JavaScript from '@mui/icons-material/DeviceHub';
import * as React from 'react'; 
import Navbar from '../components/Navbar';
import styles from '../css/StudentSearch.module.css';



export default function StudentSearch(){
    let listOfPapers=[
        {
            name:"Facial detection",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:"Euristic algorithms",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:" Image classification",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:"Object detection",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:"Complex algorithms",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        }
    ]

    let listOfPapers2=[
        {
            name:"Skin Disease",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:"Eye Detection",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:"Neural network",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:"Object detection",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:"Complex algorithms",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        }
    ]

    let listOfPapers3=[
        {
            name:"Special networking",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:"Euristic algorithms",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:" Image classification",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:"Object detection",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        },
        {
            name:"Complex algorithms",
            professor:"Liviu Mafteiu Scai",
            facultate:"Mathematics and CS",
            specializare:"Informatics",
            tehnologii:"NodeJS, ReactJS"
        }
    ]

    let [search, setSearch] = React.useState();


    const Card= ({title, teacher, faculty, specialization, technologies}) => {
        return(
            <div className={styles.card} onClick={() => {
                let message = `Hello mrs/ms ${teacher}. I like this theme: ${title}. Is it still available?`
                window.location.href = `mailto:${teacher}@e-uvt.ro?body=${message}`
            }}>
            <div className={styles.title}>
                <p>{title}</p>
            </div>
            <div className={styles.divider}></div>
            <div className={styles.checked}>
                <Person style={{color:'#4d79ff'}}/>
                <p>Teacher: {teacher}</p>
            </div>
            <div className={styles.smallDivider}></div>
            <div className={styles.checked}>
                <School style={{color:'red'}}/>
                <p>Faculty: {faculty}</p>
            </div>
            <div className={styles.smallDivider}></div>
            <div className={styles.checked}>
                <Computer style={{color:'greenyellow'}}/>
                <p>Specialization: {specialization}</p>
            </div>
            <div className={styles.smallDivider}></div>
            <div className={styles.checked}>
                <JavaScript style={{color:'green'}}/>
                <p>Technologies: {technologies}</p>
            </div>
        </div>
        )  
    };
    
    return(
        <div className={styles.page}>
            <div className={styles.importedNavbar}>
                <Navbar page="search" type="student"/>
            </div>

            <div className={styles.StudentSearchBody}>
                <div className={styles.searchBar}>
                    <Search className={styles.searchImage}/>
                    <input placeholder='Look for the  name of a paper' className={styles.searchBox} onChange={(e)=>{setSearch(e.target.value)}}/>
                </div>

                <div className={styles.listOfCards}>
                    {
                        search ? 
                        (listOfPapers.filter((element)=>element.name.toLocaleLowerCase().includes(search.toLocaleLowerCase()))
                        .map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare} 
                            technologies={element.tehnologii}
                        /> )) 
                        : 
                        (listOfPapers.map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare}
                            technologies={element.tehnologii}
                        />))
                    }
                </div>
                <div className={styles.listOfCards}>
                    {
                        search ? 
                        (listOfPapers2.filter((element)=>element.name.toLocaleLowerCase().includes(search.toLocaleLowerCase()))
                        .map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare} 
                            technologies={element.tehnologii}
                        /> )) 
                        : 
                        (listOfPapers2.map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare}
                            technologies={element.tehnologii}
                        />))
                    }
                </div>
                <div className={styles.listOfCards}>
                    {
                        search ? 
                        (listOfPapers3.filter((element)=>element.name.toLocaleLowerCase().includes(search.toLocaleLowerCase()))
                        .map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare} 
                            technologies={element.tehnologii}
                        /> )) 
                        : 
                        (listOfPapers3.map(element => 
                        <Card 
                            title={element.name} 
                            teacher={element.professor} 
                            faculty={element.facultate} 
                            specialization={element.specializare}
                            technologies={element.tehnologii}
                        />))
                    }
                </div>
            </div>
        </div>
    )

}