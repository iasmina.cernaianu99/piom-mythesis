import * as React from 'react'; 
import styles from '../css/StudentValidate.module.css';
import Navbar from '../components/Navbar';
import { Group, Text, useMantineTheme, Loader  } from '@mantine/core';
import { IconUpload, IconX } from '@tabler/icons';
import { Dropzone, PDF_MIME_TYPE } from '@mantine/dropzone';
import PictureAsPdf  from '@mui/icons-material/PictureAsPdf';
import Check from '@mui/icons-material/Check';
import  CopyAll from '@mui/icons-material/CopyAll';
import  Edit  from '@mui/icons-material/Edit';
export default function StudentValidate(){

    const theme = useMantineTheme();
    let [loading, setLoading] = React.useState(false)
    let [file, setFile] = React.useState();
    let [isVerified, setIsVerified] = React.useState(false);
    let [loader, setLoader] = React.useState(true);

    function handleAcceptDroppedFiled(files){
        setTimeout(() => {
        setLoading(false); 
        setFile(files[0].name)
        }, 2000 ); 
    }

    function verifyPlagiarism(){
        setIsVerified(true);
        setTimeout(() => {setLoader(false)}, 2000);

    }


    return(
        <div className={styles.page}>
            <div className={styles.importedNavbar}>
                <Navbar page="validate" type="student"/>
            </div>

            <div className={styles.body}>
                <div className={styles.dropzone}>
                    <Dropzone
                        loading={loading}
                        onDrop={(files) =>{setLoading(true); handleAcceptDroppedFiled(files)}}
                        onReject={(files) => console.log('rejected files', files)}
                        maxSize={3 * 1024 ** 2}
                        accept={PDF_MIME_TYPE} >
                            <Group position="center" spacing="xl" style={{ minHeight: 220, pointerEvents: 'none' }}>
                                <Dropzone.Accept>
                                <IconUpload
                                    size={50}
                                    stroke={1.5}
                                    color={theme.colors[theme.primaryColor][theme.colorScheme === 'dark' ? 4 : 6]}
                                />
                                </Dropzone.Accept>
                                <Dropzone.Reject>
                                <IconX
                                    size={50}
                                    stroke={1.5}
                                    color={theme.colors.red[theme.colorScheme === 'dark' ? 4 : 6]}
                                />
                                </Dropzone.Reject>
                                <Dropzone.Idle>
                                    {file ? <PictureAsPdf style={{height:'50px', width:'50px'}} /> : <IconUpload size={50} stroke={1.5} />}
                                
                                </Dropzone.Idle>

                                <div>
                                <Text size="xl" inline>
                                    {file ? file : "Drag or click to select a file." }
                                    
                                </Text>
                                {file ?null: <Text size="sm" color="dimmed" inline mt={7}>
                                    The attached file should not exceed 5mb.
                                </Text> }
                                
                                </div>
                            </Group>
                    </Dropzone>
                </div>
                <div className={file ? styles.verify : styles.verifyDisable} onClick={() => {verifyPlagiarism()}}>
                    <p>Verify plagiarism</p>
                </div>

                {
                    isVerified && loader ? 
                    <Loader style={{marginTop:'2%'}}/> 
                    : 
                    <div className={ isVerified ? styles.output : styles.outputDisable}>
                    <div className={styles.outputHeader}>
                        <div className={styles.blueDash}></div>
                        <div className={styles.headerTitle}> RESULT </div>
                        <div className={styles.blueDash}></div>
                    </div>

                    <div className={styles.percentsSection}>
                        <div className={styles.underPercentSection}>
                            <Check style={{color:'#4d79ff'}}/>
                            <p style={{textDecoration:'underline 2px #4d79ff ', textUnderlineOffset:'10px'}}>Checked</p>
                            <p style={{color:'#4d79ff', fontWeight:'bold'}}>100%</p>
                        </div>
                        <div className={styles.underPercentSection}>
                            <CopyAll style={{color:'red'}}/>
                            <p style={{textDecoration:'underline 2px lightgray ', textUnderlineOffset:'10px'}}>Plagiarism</p>
                            <p style={{color:'red', fontWeight:'bold'}}>20%</p>
                        </div>
                        <div className={styles.underPercentSection}>
                            <Edit style={{color:'lightgreen'}}/>
                            <p style={{textDecoration:'underline 2px lightgreen ', textUnderlineOffset:'10px'}}>Original</p>
                            <p style={{color:'lightgreen', fontWeight:'bold'}}>80%</p>
                        </div>
                    </div>


                    <div className={styles.exampleSection}>
                        <div className={styles.example}>
                            <div className={styles.plagiarizedDiv}>Plagiarized</div>
                            <div>Lorem ipsum Loresm ipsum Lorem ipsum</div>
                        </div>
                        <div className={styles.example}>
                            <div className={styles.uniqueDiv}>Unique</div>
                            <div>Lorem ipsum Loresm ipsum Lorem ipsum</div>
                        </div>
                    </div>
                </div>
                }

            </div>
        </div>
    );
}