import Search  from '@mui/icons-material/Search';
import PhoneDisabled  from '@mui/icons-material/HighlightOff';
import Send  from '@mui/icons-material/SendOutlined';
import Person  from '@mui/icons-material/Person';
import Videocam  from '@mui/icons-material/VideocamOutlined';
import * as React from 'react'; 
import Navbar from '../components/Navbar';
import styles from '../css/StudentChat.module.css';
import Webcam from "react-webcam";

export default function StudentChat(){
    const ScaiMessages = [
        's~Am incarcat tema',
        'p~OK. Am sa ma uit',
        's~Va multumesc',
        'p~O seara placuta, domnule Student!',
    ];
    const MicotaMessages = [
        's~Buna Seara!',
        'p~Salutare. Cu ce te pot ajuta?',
        's~As vrea sa ma inscriu la dumneavoastra pentru dizertatie',
        'p~Sigur ca da, vorbim maine mai multe.',
    ];

    let [person, setPerson ] = React.useState('Scai');
    let [chatMessages, setChatMessages] = React.useState(ScaiMessages);
    let [camera, setCamera] = React.useState(false);
    let [profSearch, setProfSearch] = React.useState('');
    let profesori = ["Scai", "Micota"];
    let inputMessage='';



    const ConversationHead = ({name, lastMessage}) => {
        return (
            <div className={styles.conversationWrapper} onClick={() => {setPerson(name); setChatMessages((name === 'Scai' ? ScaiMessages : MicotaMessages))}}>  
                <Person style={{width:'50px', height:'50px', backgroundColor:'#99ccff', borderRadius:'28px', padding:'3px'}}/>
                <div className={styles.conversationDetails}>
                    <p style={{fontWeight:'bold', fontSize:'20px'}}>{name}</p>
                    <p style={{marginTop:'-15px'}}>{lastMessage}</p>
                </div>

            </div>
        )
    }

    const Conversation = ({name, messages}) => {
        return(
            <div className={styles.wholeConversation}>
                <div className={styles.conversationHeader}>
                    <div className={styles.headerLeftSection}>
                        <div className={styles.available}></div>
                        <div className={styles.conversationName}>{name}</div>
                    </div>
                    <Videocam className={styles.videocam} onClick={() => {
                        setCamera(!camera);
                    }}/>
                </div>

                <div className={styles.conversationMessages}>
                    {
                        messages.map((message, index) => {
                            return(
                                <div className={message.split('~')[0]  === 's' ? styles.studentMessage : styles.professorMessage}> {message.split('~')[1]} </div>
                            )
                        })
                    }
                </div>

                <div className={styles.conversationInput}>
                    <input className={styles.messageInput} placeholder='Type your message here...' onChange={(e)=>{inputMessage = e.target.value}}
                     onKeyPress={(e) => (e.key === 'Enter' && inputMessage !== '' ? 
                     setChatMessages([...chatMessages, `s~${inputMessage}`])
                     :
                     console.log("No message") )}
                     />
                    <Send className={styles.sendMessage} onClick={() => {
                        inputMessage !== '' ?
                        setChatMessages([...chatMessages, `s~${inputMessage}`])
                        :
                        console.log("No message")
                    }} 
                   />
                    
                </div>
            </div>
        )
    }



    return(
        <div className={styles.page}>
            <div className={styles.navbar}>
            <Navbar page="chat" type="student"/>
            </div>
            <div className={styles.body}>
                <div className={styles.conversationsList}> 
                    <div className={styles.searchBar}>
                        <Search className={styles.inputIcon}/>
                        <input className={styles.inputSearch} placeholder='Search for a person' onChange={(e)=>{setProfSearch(e.target.value)}}/>
                    </div>
                    <div className={styles.conversations}>
                        {
                            profSearch!=='' && profesori[0].toLocaleLowerCase().includes(profSearch.toLocaleLowerCase()) ? 
                            <ConversationHead name="Scai" lastMessage={person === 'Scai' ? chatMessages[chatMessages.length -1].split('~')[1]: ScaiMessages[ScaiMessages.length -1].split('~')[1]}  /> 
                            : profSearch === '' ? 
                            <ConversationHead name="Scai" lastMessage={person === 'Scai' ? chatMessages[chatMessages.length -1].split('~')[1]: ScaiMessages[ScaiMessages.length -1].split('~')[1]}  /> 
                            :
                            null
                        }

                        {
                            profSearch!=='' && profesori[1].toLocaleLowerCase().includes(profSearch.toLocaleLowerCase()) ? 
                            <ConversationHead name="Micota" lastMessage={person === 'Micota' ? chatMessages[chatMessages.length -1].split('~')[1]: MicotaMessages[MicotaMessages.length -1].split('~')[1]}/>
                            : profSearch === '' ?
                            <ConversationHead name="Micota" lastMessage={person === 'Micota' ? chatMessages[chatMessages.length -1].split('~')[1]: MicotaMessages[MicotaMessages.length -1].split('~')[1]}/>
                            :
                            null
                        }                        
                       
                       
                    </div>
                </div>
                <div className={styles.selectedConversation}>
                    <Conversation name={person} messages={chatMessages}/>
                </div>
            </div>

            {camera ? <div className={styles.cameraDialog}>
            <Webcam width="450" height = "450"/>
            <PhoneDisabled style={{color:'red', width:'40px', height:'40px', cursor:'pointer'}} onClick={() => {setCamera(!camera)}}/>
                
            </div>: null}
        </div>
    )
}