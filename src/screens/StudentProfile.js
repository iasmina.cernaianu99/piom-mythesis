import * as React from 'react'; 
import styles from '../css/StudentProfile.module.css';
import Navbar from '../components/Navbar';
import { Group, Text, useMantineTheme, Loader  } from '@mantine/core';
import { IconUpload, IconX } from '@tabler/icons';
import { Dropzone, PDF_MIME_TYPE } from '@mantine/dropzone';
import PictureAsPdf  from '@mui/icons-material/PictureAsPdf';
import Check from '@mui/icons-material/Check';
import  CopyAll from '@mui/icons-material/CopyAll';
import  Edit  from '@mui/icons-material/Edit';
import Person from '@mui/icons-material/Person';
import PersonOutline from '@mui/icons-material/PersonOutline';
import School from '@mui/icons-material/SchoolOutlined';
import ComputerOutlined from '@mui/icons-material/ComputerOutlined';
import ArticleOutlined  from '@mui/icons-material/ArticleOutlined';
import AccessTimeOutlined  from '@mui/icons-material/AccessTimeOutlined';
import ContactPageOutlined  from '@mui/icons-material/ContactPageOutlined';
import PriorityHigh  from '@mui/icons-material/PriorityHigh';
import OpenInNew  from '@mui/icons-material/OpenInNew';
import file from '../assets/file.pdf'

export default function StudentProfile(){

    const theme = useMantineTheme();
    let [loading, setLoading] = React.useState(false)
    let [files, setFiles] = React.useState();

    const handleSendFiles = () => {
        let link = "mailto: sebastian.stefaniga@yahoo.com"
             + "?cc=iasmina.cernaianu99@e-uvt.ro"
             + "&subject=Documents Signed"
             + "&body=Buna ziua domnule profesor! Am semnat urmatoarele acte si le puteti descarca de pe platorma: " + files.map((file, index) => {return `${index+1}.${file}`});
        window.location.href = link;
    }
    
    function handleAcceptDroppedFiled(listOfFiles){
        let filesNames = [];
        listOfFiles.forEach((file) => {
            filesNames.push(file.name)
        })
        setTimeout(() => {
        setLoading(false); 
        setFiles(filesNames)
        }, 2000 ); 
    }

    return(
        <div className={styles.page}>
            <div className={styles.importedNavbar}>
                <Navbar type="student"/>
            </div>
            <div className={styles.body}>

                <div className={styles.profilePicture}>
                    <Person style={{width:'100px', height:'100px', color:'#4d79ff'}}/>
                </div>
                <div className={styles.studentInfo}>
                    <div className={styles.innerStudentInfo}>
                        <div className={styles.info}>
                            <div className={styles.imageText}>
                                <PersonOutline style={{color:'#4d79ff'}}/>
                                <p style={{fontWeight:'bold', fontSize:'17px'}}>Name:</p>
                            </div>
                            <p>Cernaianu Iasmina</p>
                        </div>
                        <div className={styles.info}>
                            <div className={styles.imageText}>
                                <School style={{color:'#4d79ff'}}/>
                                <p style={{fontWeight:'bold', fontSize:'17px'}}>Faculty:</p>
                            </div>
                            <p>Mathematics and Informatics</p>
                        </div>
                        <div className={styles.info}>
                            <div className={styles.imageText}>
                                <ComputerOutlined style={{color:'#4d79ff'}}/>
                                <p style={{fontWeight:'bold', fontSize:'17px'}}>Specialization:</p>
                            </div>
                            <p>Computer Science</p>
                        </div>
                        <div className={styles.info}>

                        </div>
                    </div>

                    <div className={styles.innerStudentInfo}> 
                        <div className={styles.info}>
                                <div className={styles.imageText}>
                                    <ContactPageOutlined style={{color:'#4d79ff'}}/>
                                    <p style={{fontWeight:'bold', fontSize:'17px'}}>Series:</p>
                                </div>
                                <p>IA</p>
                            </div>
                            <div className={styles.info}>
                                <div className={styles.imageText}>
                                    <PersonOutline style={{color:'#4d79ff'}}/>
                                    <p style={{fontWeight:'bold', fontSize:'17px'}}>Identification number:</p>
                                </div>
                                <p>1629</p>
                            </div>
                            <div className={styles.info}>
                                <div className={styles.imageText}>
                                    <Edit style={{color:'#4d79ff'}}/>
                                    <p style={{fontWeight:'bold', fontSize:'17px'}}>Year of study:</p>
                                </div>
                                <p>2</p>
                            </div>
                            <div className={styles.info}>

                            </div>
                    </div>
                </div>

                <div className={styles.sectionDivider}></div>
                <div className={styles.thesisInfo}>
                    <div className={styles.thesisHeader}>
                        <h3 style={{color:'#4d79ff'}}>My thesis</h3>
                    </div>
                    <div className={styles.workInfo}>
                    <div className={styles.innerStudentInfo}> 
                        <div className={styles.info}>
                                <div className={styles.imageText}>
                                    <ArticleOutlined style={{color:'#4d79ff'}}/>
                                    <p style={{fontWeight:'bold', fontSize:'17px'}}>Chosen theme:</p>
                                </div>
                                <p>Image Detection</p>
                            </div>
                            <div className={styles.info}>
                                <div className={styles.imageText}>
                                    <PersonOutline style={{color:'#4d79ff'}}/>
                                    <p style={{fontWeight:'bold', fontSize:'17px'}}>Coordinator:</p>
                                </div>
                                <p>Stefaniga Sebastian</p>
                            </div>
                            <div className={styles.info}>
                                <div className={styles.imageText}>
                                    <AccessTimeOutlined style={{color:'#4d79ff'}}/>
                                    <p style={{fontWeight:'bold', fontSize:'17px'}}>Time left:</p>
                                </div>
                                <p>12 weeks</p>
                            </div>
                            <div className={styles.info}>

                            </div>
                    </div>
                    <div className={styles.innerStudentInfo}> 
                        <div className={styles.info}>
                                <div className={styles.imageText}>
                                    <Edit style={{color:'#4d79ff'}}/>
                                    <p style={{fontWeight:'bold', fontSize:'17px'}}>Drafts sent:</p>
                                </div>
                                <p>3</p>
                            </div>
                            <div className={styles.info}>
                                <div className={styles.imageText}>
                                    <CopyAll style={{color:'#4d79ff'}}/>
                                    <p style={{fontWeight:'bold', fontSize:'17px'}}>Plagiarism:</p>
                                </div>
                                <p>17%</p>
                            </div>
                            <div className={styles.info}>
                                <div className={styles.imageText}>
                                    <Check style={{color:'#4d79ff'}}/>
                                    <p style={{fontWeight:'bold', fontSize:'17px'}}>Professor approval:</p>
                                </div>
                                <p>Yes</p>
                            </div>
                            <div className={styles.info}>

                            </div>
                    </div>

                </div>
                </div>
                <div className={styles.sectionDivider}></div>
                <div className={styles.documentsSection}>
                    <div className={styles.documentsHeader}>
                        <h3 style={{color:'#4d79ff'}}>Documents</h3>
                    </div>
                    <div className={styles.note}>
                        <PriorityHigh style={{color:'red'}}/>
                        <p style={{fontWeight:'bold'}}>Please download, sign and then upload following signed documents on the platform:</p>
                    </div>
                    
                    <div className={styles.listOfDocuments}>
                        <ul style={{width:'120px'}}>
                            <div onClick={() => {window.open(file)}} style={{display:'flex', flexDirection:'row', cursor:'pointer'}}><li >Document1.pdf</li><OpenInNew style={{color:'#4d79ff'}}/></div>
                            <div onClick={() => {window.open(file)}} style={{display:'flex', flexDirection:'row', cursor:'pointer'}}><li >Document2.pdf</li><OpenInNew style={{color:'#4d79ff'}}/></div>
                            <div onClick={() => {window.open(file)}} style={{display:'flex', flexDirection:'row', cursor:'pointer'}}><li >Document3.pdf</li><OpenInNew style={{color:'#4d79ff'}}/></div>
                        </ul>
                    </div>

                    <div className={styles.uploadSection}>
                    <Dropzone
                        style={{width:'100%'}}
                        loading={loading}
                        onDrop={(files) =>{setLoading(true); handleAcceptDroppedFiled(files)}}
                        onReject={(files) => console.log('rejected files', files)}
                        maxSize={3 * 1024 ** 2}
                        accept={PDF_MIME_TYPE} >
                            <Group position="center" spacing="xl" style={{ minHeight: 220, pointerEvents: 'none' }}>
                                <Dropzone.Accept>
                                <IconUpload
                                    size={50}
                                    stroke={1.5}
                                    color={theme.colors[theme.primaryColor][theme.colorScheme === 'dark' ? 4 : 6]}
                                />
                                </Dropzone.Accept>
                                <Dropzone.Reject>
                                <IconX
                                    size={50}
                                    stroke={1.5}
                                    color={theme.colors.red[theme.colorScheme === 'dark' ? 4 : 6]}
                                />
                                </Dropzone.Reject>
                                <Dropzone.Idle>
                                    {files ? <PictureAsPdf style={{height:'50px', width:'50px'}} /> : <IconUpload size={50} stroke={1.5} />}
                                
                                </Dropzone.Idle>

                                <div>
                                <Text size="xl" inline>
                                    {files ? files.map((file) =>{return `${file};   `} ) : "Drag or click to select a file." }
                                    
                                </Text>
                                {files ?null: <Text size="sm" color="dimmed" inline mt={7}>
                                    The attached file should not exceed 5mb.
                                </Text> }
                                
                                </div>
                            </Group>
                    </Dropzone>
                    </div>

                    
                    <div className={ files ?  styles.sendDocuments : styles.sendDocumentsDisabled } onClick={handleSendFiles}><p>Send documents</p></div> 
                    
                </div>
            </div>
        </div>
    )
}