import * as React from 'react'; 
import '../css/Register.css';
import { useNavigate } from 'react-router-dom';
import Email from '@mui/icons-material/Email';
import Person from '@mui/icons-material/Person';
import Password from '@mui/icons-material/Password';

export default function Register(){
    let navigate = useNavigate();
    let [student, setStudent] =  React.useState(false); 
    let [proffesor, setProffesor] = React.useState(false);
    let [email, setEmail] = React.useState(); 
    let [name, setName] = React.useState(); 
    let [password, setPassword] = React.useState();
    let [emailError, setEmailError] = React.useState(false);
    let [nameError, setNameError] = React.useState(false);
    let [passwordError, setPasswordError] = React.useState(false);
    let [studentError, setStudentError] = React.useState(false);
    let [proffesorError, setProffesorError] = React.useState(false);

    function submitRegisterForm(){
        let typeOfUser = null; 
        if(student){
            typeOfUser = 'student'
        }
        if(proffesor){
            typeOfUser = 'proffesor'
        }

        console.log(typeOfUser, email, name, password)

        if(typeOfUser && email && name && password){
            let path = '/';
            navigate(path);
        }else{
            console.log("Date incorecte");

            if(!typeOfUser){
                setStudentError(true);
                setProffesorError(true);
            }

            if(!email){
                setEmailError(true)
            }

            if(!name){
                setNameError(true)
            }

            if(!password){
                setPasswordError(true)
            }

            
            
        }
        
        
    }


    return(
        <div className='screen'>
            <div className='registerCard'>
                <div className='titleSection'>
                    <p className='title'>Registration</p>
                </div>
                <div className='dividerBetweenSections'></div>
                <div className='firstSection'>
                    <div className='typeOfUser'>
                        <input type={'checkbox'} id='student' onChange={(e) => {setStudentError(false); setProffesorError(false); setStudent(e.target.checked)}}/>
                        <p style={studentError ? {color: 'red'} : {color:'black'}}>Student</p>
                    </div>
                    <div className='typeOfUser'>
                        <input type='checkbox' id='teacher' onChange={(e) => {setStudentError(false); setProffesorError(false); setProffesor(e.target.checked)}}/>
                        <p style={proffesorError ? {color: 'red'} : {color:'black'}}>Teacher/Researcher</p>
                    </div>
                </div>
                <div className='dividerBetweenSections'></div>
                <div className='secondSection'>
                    <div className={emailError ? 'email error' : 'email' }>
                        <Email style={{ backgroundColor: "#4d79ff" , color:"white", marginRight:'-10%', zIndex:1, height:'34px', width:'10%', borderBottomLeftRadius:'5px', borderTopLeftRadius:'5px'}}/>
                        <input type={"text"} placeholder="Email" id = "email" onChange={(e) => { setEmailError(false); setEmail(e.target.value)}}/>
                    </div>
                    <div className={nameError ? 'name error': 'name'}>
                        <Person style={{ backgroundColor: "#4d79ff" , color:"white", marginRight:'-10%', zIndex:1, height:'34px', width:'10%', borderBottomLeftRadius:'5px', borderTopLeftRadius:'5px'}}/>
                        <input type={"text"} placeholder="Name" id='name' onChange={(e) => {setNameError(false); setName(e.target.value)}}/>
                    </div>
                    <div className={ passwordError ?  'password error' : 'password'}>
                        <Password style={{ backgroundColor: "#4d79ff" , color:"white", marginRight:'-10%', zIndex:1, height:'34px', width:'10%', borderBottomLeftRadius:'5px', borderTopLeftRadius:'5px'}}/>
                        <input type={"password"} placeholder="Password" id='password' onChange={(e) => { setPasswordError(false); setPassword(e.target.value)}}/>
                    </div>
                </div>
                <div className='dividerBetweenSections'></div>
                <div className='submitSection' onClick={() =>{submitRegisterForm()}}>
                    Submit
                </div>
            </div>
        </div>
    )
}