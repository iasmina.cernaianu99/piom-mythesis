import * as React from 'react'; 
import { useNavigate } from 'react-router-dom';
import Navbar from '../components/Navbar';
import styles from '../css/ProffesorPublish.module.css';

export default function ProffesorPublish(){
    let navigate = useNavigate();
    let [fullname, setFullName] =  React.useState(); 
    let [themename, setThemeName] = React.useState();
    let [faculty, setFaculty] = React.useState(); 
    let [specialization, setSpecialization] = React.useState(); 
    let [description, setDescription] = React.useState();
    let [technologies, setTehnologies] = React.useState();
    let [bibliography, setBibliography] = React.useState();
    let [fullnameError, setFullNameError] = React.useState(false);
    let [themenameError, setThemeNameError] = React.useState(false);
    let [facultyError, setFacultyError] = React.useState(false);
    let [specializationError, setSpecializationError] = React.useState(false); 
    let [descriptionError, setDescriptionError] = React.useState(false);
    let [technologiesError, setTehnologiesError] = React.useState(false);
    let [bibliographyError, setBibliographyError] = React.useState(false);

    function submitPublishForm() {

            if(!fullname){
                setFullNameError(true);
            }
            if(!themename){
                setThemeNameError(true)
            }
            if(!faculty){
                setFacultyError(true)
            }
            if(!specialization){
                setSpecializationError(true)
            }
            if(!description){
                setDescriptionError(true)
            }
            if(!technologies){
                setTehnologiesError(true)
            }
            if(!bibliography){
                setBibliographyError(true)
            }

            if( fullname && themename && faculty && specialization && description && technologies && bibliography){
                console.log('da');
                setFullName('');
                setThemeName('');
                setFaculty('')
                setSpecialization('');
                setDescription('');
                setTehnologies('');
                setBibliography('');
                window.location.reload(false);
            }
        }

    return(
        <div className={styles.page}>
            <div className={styles.importedNavbar}>
                <Navbar page='publish' type="proffesor" />
            </div>

            <div className={styles.body}>
                <div className={styles.form}>
                    <div className={styles.title}> Publish a new theme</div>

                    <div id="form" className={styles.content}>
                        <input placeholder='Your full name' onChange={(e) => {setFullNameError(false); setFullName(e.target.value)}} style={fullnameError ? {borderBottom: '2px solid red'} : {borderBottom: '2px solid #4d79ff'}}/>
                        <input placeholder='Theme name' onChange={(e) => {setThemeNameError(false); setThemeName(e.target.value)}} style={themenameError ? {borderBottom: '2px solid red'} : {borderBottom: '2px solid #4d79ff'}}/>
                        <input placeholder='Faculty' onChange={(e) => {setFacultyError(false); setFaculty(e.target.value)}} style={facultyError ? {borderBottom: '2px solid red'} : {borderBottom: '2px solid #4d79ff'}}/>
                        <input placeholder='Specialization' onChange={(e) => {setSpecializationError(false); setSpecialization(e.target.value)}} style={specializationError ? {borderBottom: '2px solid red'} : {borderBottom: '2px solid #4d79ff'}}/>
                        <input placeholder='Description' onChange={(e) => {setDescriptionError(false); setDescription(e.target.value)}} style={descriptionError ? {borderBottom: '2px solid red'} : {borderBottom: '2px solid #4d79ff'}}/>
                        <input placeholder='Techonologies' onChange={(e) => {setTehnologiesError(false); setTehnologies(e.target.value)}} style={technologiesError ? {borderBottom: '2px solid red'} : {borderBottom: '2px solid #4d79ff'}}/>
                        <input placeholder='Bibliography' onChange={(e) => {setBibliographyError(false); setBibliography(e.target.value)}} style={bibliographyError ? {borderBottom: '2px solid red'} : {borderBottom: '2px solid #4d79ff'}}/>
                    </div>

                    <div className={styles.submitSection}>
                        <p style={{fontSize:'15px', fontWeight:'bold'}} onClick={() => submitPublishForm()}>Submit</p>
                    </div>
                </div>
            </div>
        </div>
    )
}