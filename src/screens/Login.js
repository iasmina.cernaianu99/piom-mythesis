import * as React from 'react'; 
import styles from '../css/Login.module.css';
import '../css/Login.module.css';
import { Link, useNavigate } from 'react-router-dom';
import Email from '@mui/icons-material/Email';
import Password from '@mui/icons-material/Password';
import logo from '../assets/user.png'

export default function Login(){

    const navigate = useNavigate(); 
    let [email, setEmail] = React.useState(); 
    let [password, setPassword] = React.useState();
    let [emailError, setEmailError] = React.useState(false);
    let [passwordError, setPasswordError] = React.useState(false);

    function submitLogIn(){

        if(email && password){
            let path; 
            if(email.includes('professor')){
                path = '/proffesorHome'
            }else{
                path = '/studentHome'
            }
            navigate(path);
        }else{
            if(!email){
                setEmailError(true);
            }

            if(!password){
                setPasswordError(true);
            }
        }
    }


    return(
        <div className={styles.screen}>
            <div className={styles.registerCard}>
                <div className={styles.iconForTitle}>
                    <img className={styles.titleImage} src={logo}/>
                </div>
                <div className={styles.titleSection}>
                    <p className={styles.title}>Login</p>
                </div>
                <div className={styles.dividerBetweenSections}></div>
                <div className={styles.firstSection}>
                    <div className={emailError ? styles.error : styles.email}>
                        <Email style={{ backgroundColor: "#4d79ff" , color:"white", marginRight:'-10%', zIndex:1, height:'34px', width:'10%', borderBottomLeftRadius:'5px', borderTopLeftRadius:'5px'}}/>
                        <input type={"text"} placeholder="Email" onChange={(e) =>{
                            setEmailError(false);
                            setEmail(e.target.value);
                        }}/>
                    </div>
                    <div className={passwordError ? styles.error : styles.password}>
                        <Password style={{ backgroundColor: "#4d79ff" , color:"white", marginRight:'-10%', zIndex:1, height:'34px', width:'10%', borderBottomLeftRadius:'5px', borderTopLeftRadius:'5px'}}/>
                        <input type={"password"} placeholder="Password" onChange={(e) => {
                            setPasswordError(false);
                            setPassword(e.target.value);
                        }}/>
                    </div>
                </div>
                <div className={styles.dividerBetweenSections}></div>
                <div className={styles.submitSection} onClick={() => {submitLogIn()}}>
                    <p style={{color:'white', fontWeight:'bold'}}>LOG IN</p>
                </div>
                <div className={styles.account}>
                    <a onClick={()=> navigate('/register')}>Don't have an account?</a>
                </div>
            </div>
        </div>
    )
}