import Create  from '@mui/icons-material/Create';
import ContentCopy  from '@mui/icons-material/ContentCopy';
import School  from '@mui/icons-material/School';
import Person  from '@mui/icons-material/Person';
import * as React from 'react'; 
import Navbar from '../components/Navbar';
import styles from '../css/ProffesorHome.module.css';
import People  from '@mui/icons-material/People';
import Computer  from '@mui/icons-material/Computer';
import Functions  from '@mui/icons-material/Functions';

export default function ProffesorHome(){


    const CardValidation = ({title, students, cs, math}) => {
        return(
            <div className={styles.card}>
            <div className={styles.title}>
                <p>{title}</p>
            </div>
            <div className={styles.divider}></div>
            <div className={styles.checked}>
                <People style={{color:'#4d79ff'}}/>
                <p>All students</p>
                <p className={styles.percents}>{students}</p>
            </div>
            <div className={styles.smallDividerCheck}></div>
            <div className={styles.checked}>
                <Computer style={{color:'red'}}/>
                <p>Computer Science</p>
                <p className={styles.percentsPlagiarism}>{cs}</p>
            </div>
            <div className={styles.smallDividerPlagiarism}></div>
            <div className={styles.checked}>
                <Functions style={{color:'greenyellow'}}/>
                <p>Mathematics</p>
                <p className={styles.percentsOriginal}>{math}</p>
            </div>
            <div className={styles.smallDividerOriginal}></div>
        </div>
        )  
    };

    const CardComment = ({title, proffesor, student}) => {
        return(
            <div className={styles.card}>
            <div className={styles.title}>
                <p>{title}</p>
            </div>
            <div className={styles.divider}></div>
            {proffesor.map((comment, index)=>{
                return (
                <div style={{width:'100%', display:'flex'}}>
                    <div className={styles.commentSection}>
                        <School style={{color:'green'}}/>
                        <div>Student: {student[index]}</div>
                    </div>
                    <div className={styles.commentSection}>
                        <Person style={{color:'#4d79ff'}}/>
                        <div>You: {comment}</div>
                    </div>
                    
                </div>
               
                )
            })}
        </div>
        )  
    };

    return(
        <div className={styles.page}>
            <div className={styles.importedNavbar}>
                <Navbar page="home" type="proffesor"/>
            </div>

            <div className={styles.bodyHome}>
            <h1>Summary of published themes</h1>
                <div className={styles.cardSection}>
                <CardValidation title="Object Detection" students="15" cs="10" math="5"/>
                <CardValidation title="Image classification"  students="30" cs="21" math="9"/>
                <CardValidation title="Complex algorithms"  students="16" cs="12" math="4"/>
                </div>
                <h1 style={{marginTop:'50px'}}>Questions from students</h1>
                <div className={styles.cardSection}>
                <CardComment title="Object Detection" proffesor={[
                    'You can improve the documentation.',
                    'The paper overall looks pretty good.',
                    'I think you could change the code a little bit to make it reusable.'
                ]} student={[
                    'OK, will do.',
                    'Thank you!',
                    'Sure,  will take care of it.'
                ]}/>
                <CardComment title="Image classification" proffesor={[
                    'Seems to be ok.'
                ]} student={[
                    'Great!'
                ]}/>
                <CardComment title="Complex algorithms" proffesor={[
                    'It looks good.',
                    "I look forward to the discussion next week."
                ]} student={[
                    'Thank you!',
                    'So do I.'
                ]}/>
                </div>
            </div>
        </div>
    )

}