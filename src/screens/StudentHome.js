import Create  from '@mui/icons-material/Create';
import Check  from '@mui/icons-material/Check';
import ContentCopy  from '@mui/icons-material/ContentCopy';
import School  from '@mui/icons-material/School';
import Person  from '@mui/icons-material/Person';
import * as React from 'react'; 
import Navbar from '../components/Navbar';
import styles from '../css/StudentHome.module.css';

export default function StudentHome(){


    const CardValidation = ({title, plagiarism, original}) => {
        return(
            <div className={styles.card}>
            <div className={styles.title}>
                <p>{title}</p>
            </div>
            <div className={styles.divider}></div>
            <div className={styles.checked}>
                <Check style={{color:'#4d79ff'}}/>
                <p>Checked</p>
                <p className={styles.percents}>100%</p>
            </div>
            <div className={styles.smallDividerCheck}></div>
            <div className={styles.checked}>
                <ContentCopy style={{color:'red'}}/>
                <p>Plagiarism</p>
                <p className={styles.percentsPlagiarism}>{plagiarism}</p>
            </div>
            <div className={styles.smallDividerPlagiarism}></div>
            <div className={styles.checked}>
                <Create style={{color:'greenyellow'}}/>
                <p>Original</p>
                <p className={styles.percentsOriginal}>{original}</p>
            </div>
            <div className={styles.smallDividerOriginal}></div>
        </div>
        )  
    };

    const CardComment = ({title, proffesor, student}) => {
        return(
            <div className={styles.card}>
            <div className={styles.title}>
                <p>{title}</p>
            </div>
            <div className={styles.divider}></div>
            {proffesor.map((comment, index)=>{
                return (
                <div style={{width:'100%', display:'flex'}}>
                    <div className={styles.commentSection}>
                        <Person style={{color:'#4d79ff'}}/>
                        <div>Teacher: {comment}</div>
                    </div>
                    <div className={styles.commentSection}>
                        <School style={{color:'green'}}/>
                        <div>You: {student[index]}</div>
                    </div>
                </div>
               
                )
            })}
        </div>
        )  
    };

    return(
        <div className={styles.page}>
            <div className={styles.importedNavbar}>
                <Navbar page="home" type="student"/>
            </div>

            <div className={styles.bodyHome}>
            <h1>Results of draft validation</h1>
                <div className={styles.cardSection}>
                <CardValidation title="Draft 1" plagiarism="20%" original="80%"/>
                <CardValidation title="Draft 2" plagiarism="11%" original="89%"/>
                <CardValidation title="Draft 3" plagiarism="2%" original="98%"/>
                </div>
                <h1 style={{marginTop:'50px'}}>Teacher's comments of drafts</h1>
                <div className={styles.cardSection}>
                <CardComment title="Draft 1" proffesor={[
                    'You can improve the documentation.',
                    'The paper overall looks pretty good.',
                    'I think you could change the code a little bit to make it reusable.'
                ]} student={[
                    'OK, will do.',
                    'Thank you!',
                    'Sure,  will take care of it.'
                ]}/>
                <CardComment title="Draft 2" proffesor={[
                    'Seems to be ok.'
                ]} student={[
                    'Great!'
                ]}/>
                <CardComment title="Draft 3" proffesor={[
                    'It looks good.',
                    "I look forward to the discussion next week."
                ]} student={[
                    'Thank you!',
                    'So do I.'
                ]}/>
                </div>
            </div>
        </div>
    )

}