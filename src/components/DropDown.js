import * as React from 'react'; 
import styles from '../css/DropDown.module.css';
import { useNavigate } from 'react-router-dom';

export default function DropDown({type, visible, x, y}){
    const navigate = useNavigate();
    return(
        <div style={visible ? {marginTop:'20px', width:'150px', height:'auto', display:'flex', flexDirection:'column', top:`${y}px`, left:`${x}px`, position:'absolute', backgroundColor:'#4d79ff', borderRadius:'5px', justifyContent:'space-evenly'} : {display:'none'}}>
            {type === 'student' ? 
            <div className={styles.dropDownOption} onClick={() => {
                if(type === 'student'){
                    navigate('/studentProfile')
                }
            }}>
                <p>MyProfile</p>
            </div>
            :
             null}
            
            <div className={styles.dropDownOption} onClick={() => {navigate('/')}}>
                <p>SignOut</p>
            </div>
        </div> 
    )
}