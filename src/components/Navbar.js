import * as React from 'react'; 
import styles from '../css/Navbar.module.css';
import Home from '@mui/icons-material/Home';
import Search from '@mui/icons-material/Search';
import DoneAll from '@mui/icons-material/DoneAll';
import Chat from '@mui/icons-material/Chat';
import Person  from '@mui/icons-material/Person';
import Publish  from '@mui/icons-material/Publish';
import People  from '@mui/icons-material/People';
import MenuBook  from '@mui/icons-material/MenuBook';
import { useNavigate } from 'react-router-dom';
import DropDown from './DropDown';

export default function Navbar({page, type}){

    const navigate = useNavigate();
    let [dropDownVisible, setDropDownVisible] = React.useState(false)
    let [x, setX] = React.useState(0);
    let [y, setY] = React.useState(0);
    let [number, setNumber] = React.useState(0);
    function optionPressed(option){
        
        if(type === 'student'){
            if(option === 'home'){
                navigate('/studentHome')
            }else if(option === 'search'){
                navigate('/studentSearch')
            }else if(option === 'validate'){
                navigate('/studentValidate')
            }else{
                navigate('/studentChat')
            }
        }else{
            if(option === 'home'){
                navigate('/proffesorHome')
            }else if(option === 'publish'){
                navigate('/proffesorPublish')
            }else if(option === 'mystudents'){
                navigate('/proffesorMyStudents')
            }else{
                navigate('/proffesorChat')
            }
        }
    }
    return(
        <div className={styles.navbar}>
            <div className={styles.logo}>
                <MenuBook />
                <h2>MyThesis</h2>
            </div>

            <div className={styles.menus}>
                <div className={ page === 'home' ? styles.selectedPage : styles.option}
                onClick={()=>{optionPressed('home')}}>
                    <Home/>
                    <p>Home</p> 
                </div>
                <div className={page === 'search' || page === 'publish' ? styles.selectedPage : styles.option}
                onClick={()=>{optionPressed( type === 'student' ? 'search' : 'publish')}}>
                    
                    {type === 'student' ? <Search/> : <Publish/>}
                    {type === 'student' ? <p>Search</p> : <p>Publish</p>}
                </div>
                <div className={page === 'validate' || page=== 'mystudents' ? styles.selectedPage : styles.option}
                onClick={()=>{optionPressed(type === 'student' ? 'validate' : 'mystudents')}}>
                    
                    {type === 'student' ? <DoneAll/> : <People/>}
                    {type === 'student' ? <p>Validate</p> : <p>My Students</p>}
                </div>

                 <div className={page === 'chat' ? styles.selectedPage : styles.option}
                onClick={()=>{optionPressed('chat')}}>
                    <Chat/>
                    <p>Chat</p> 
                </div> 
                
            </div>

            <div className={styles.dropDown} onClick={(e) => {

                setX(e.clientX);
                setY(e.clientY);
                let copy = number;
                console.log(copy)
                if(copy % 2 === 0){
                setDropDownVisible(true);
                }
                else{
                    setDropDownVisible(false)
                } 
                setNumber(copy+1);
                

                
                
            }}>
                <Person/>
            </div>

            <DropDown type={type} visible={dropDownVisible} x={x-75} y={y+30} />
        </div>
    )
}