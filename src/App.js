
import './App.css';
import { Route, Routes } from 'react-router-dom';
import Register from './screens/Register';
import StudentSearch from './screens/StudentSearch';
import Login from './screens/Login';
import StudentHome from './screens/StudentHome';
import StudentValidate from './screens/StudentValidate';
import StudentChat from './screens/StudentChat';
import ProffesorHome from './screens/ProffesorHome';
import ProffesorMyStudents from './screens/ProffesorMyStudents';
import ProffesorPublish from './screens/ProffesorPublish';
import ProffesorChat from './screens/ProffesorChat';
import StudentProfile from './screens/StudentProfile';

function App() {
  return (

      <Routes>
            
            <Route path='/proffesorChat' element={<ProffesorChat/>}/>
            <Route path='/proffesorPublish' element={<ProffesorPublish/>}/>
            <Route path='/proffesorMyStudents' element={<ProffesorMyStudents/>}/>
            <Route path='/proffesorHome' element={<ProffesorHome/>}/>
            <Route path='/studentProfile' element={<StudentProfile/>}/>
            <Route path='/studentChat' element={<StudentChat/>}/>
            <Route path='/studentValidate' element={<StudentValidate/>}/>
            <Route path='/studentHome' element={<StudentHome/>}/>
            <Route path='/register' element={<Register/>}/>
            <Route path='/studentSearch' element={<StudentSearch/>}/>
            <Route path='/' element={<Login/>}/>
      </Routes>
    
  );
}

export default App;
